<?php
/**
 * Created by PhpStorm.
 * User: luis
 * Date: 14-06-2018
 * Time: 15:40
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Exception\GuzzleException;
use PDO;

class CounterController Extends Controller {

    private $server_name;
    private $database;
    private $worten_database;
    private $username;
    private $password;

    public function __construct()
    {

        $this->middleware('auth');
        $this->server_name = env('DB_HOST');
        $this->server_name_worten = env('DB_HOST_WORTEN');
        $this->database = env('DB_DATABASE');
        $this->username = env('DB_USERNAME');
        $this->password = env('DB_PASSWORD');

    }

    public function getVenuesToCustomer(Request $request) {

        $header = array(
            'Content-Type'      => 'application/json;charset=UTF8',
            'charset'           => 'utf-8'
        );

        try {

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if ( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $venues_query = "
                  SELECT
                    Venues.VenueId AS 'venue_id',
                    Locations.Name AS 'name'
                  FROM
                    Venues
                    LEFT JOIN
                        Locations
                            ON (
                                Locations.LocationId = Venues.VenueId
                                AND
                                Locations.LocationType = '2'
                            )
                ";

                $venues_result = sqlsrv_query($conn, $venues_query);

                if ($venues_result === false) {

                    Log::info('[x] ... ERROR 600');
                    Log::info(sqlsrv_errors());

                    return response()->json(array('error' => array('Query' => 'Query Malformed')), 200);

                }

                $venues = array();

                while ( $row = sqlsrv_fetch_array( $venues_result, SQLSRV_FETCH_ASSOC)) {

                    array_push($venues, $row);

                }

                return response()->json($venues, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getVenues(Request $request) {

        $header = array(
            'Content-Type'  => 'application/json; charset=UTF8',
            'charset'       => 'utf-8'
        );

        try {

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if ( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $venues_query = "
                  SELECT
                    Venues.VenueId,
                    Locations.Name
                  FROM
                    Venues
                    LEFT JOIN
                    Locations
                    ON (Locations.LocationId = Venues.VenueId)
                ";

                $venues_result = sqlsrv_query($conn, $venues_query);

                if ($venues_result === false) {

                    Log::info('[x] ... ERROR 600');
                    Log::info(sqlsrv_errors());

                    return response()->json(array('error' => array('Query' => 'Query Malformed')), 200);

                }

                $venues = array();

                while ( $row = sqlsrv_fetch_array( $venues_result, SQLSRV_FETCH_ASSOC)) {

                    array_push($venues, $row);

                }

                return response()->json($venues, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getVenueLayouts(Request $request, $venue_id) {


        $header = array(
            'Content-Type'      => 'application/json; charset=UTF-8',
            'charset'           => 'utf-8'
        );

        try {

            $validate_venue = $this->validateVenue($venue_id);

            if (!$validate_venue || isset($validate_venue['error'])) {

                return response()->json(array('Error' => $validate_venue['error']), 200);

            }

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if ( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $layouts_query = "
                    SELECT
                        DISTINCT Layouts.LayoutId AS layout_id,
                        Locations.Name AS name
                    FROM
                        Layouts
                    LEFT JOIN
                        Locations ON (
                             Layouts.LayoutId = Locations.LocationId
                        )
                    WHERE
                        VenueId = " . $venue_id . "
                ";

                $stmt = sqlsrv_query($conn, $layouts_query);

                $layouts = array();

                while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                    array_push($layouts, $row);

                }

                sqlsrv_free_stmt( $stmt );

                return response()->json($layouts, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getTemplates(Request $request, $venue_id) {

        $header = array(
            'Content-Type'      => 'application/json; charset=UTF-8',
            'charset'           => 'utf-8'
        );

        try {

            $validate_venue = $this->validateVenue($venue_id);

            if (!$validate_venue || isset($validate_venue['error'])) {

                return response()->json(array('Error' => $validate_venue['error']), 200);

            }

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if ( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $layouts_query = "
                  SELECT
                    Layouts.LayoutId,
                    L.Name
                  FROM
                    Layouts
                    LEFT JOIN
                    Locations L on Layouts.LayoutId = L.LocationId
                  WHERE
                    VenueId = " . $venue_id . "
                ";

                $stmt = sqlsrv_query($conn, $layouts_query);

                $layouts = array();

                while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                    array_push($layouts, $row);

                }

                sqlsrv_free_stmt( $stmt );

                return response()->json($layouts, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getAreaCounter(Request $request) {

        $header = array(
            'Content-Type'  => 'application/json; charset=UTF-8',
            'charset'       => 'utf-8'
        );

	   return response()->json(array('Entries' => 41134), 200);

        try {

            $connection_info = array("LoginTimeout"=>60, "Database" => $this->database, "UID" => $this->username, "PWD" => $this->password );
            $conn = sqlsrv_connect( $this->server_name, $connection_info );

            if ($conn) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $query = "
                    SELECT
                        Counters.Entries
                    FROM
                        Counters
                    WHERE
                        Counters.CounterId = '169'
                ";

                $stmt = sqlsrv_query($conn, $query);

                if ($stmt === false) {

                    return response()->json(array('Error' => array('Query' => sqlsrv_errors())), 200);

                }

                $counter = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

                return response()->json($counter, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getWortenAreaEntryCounter(Request $request) {

        $header = array(
            'Content-Type'  => 'application/json; charset=UTF-8',
            'charset'       => 'utf-8'
        );

        try {

            $connection_info = array("LoginTimeout"=>60, "Database" => $this->database, "UID" => $this->username, "PWD" => $this->password );
            $conn = sqlsrv_connect( $this->server_name_worten, $connection_info );

            if ($conn) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $query = "
                    SELECT
                        Counters.Entries
                    FROM
                        Counters
                    WHERE
                        Counters.CounterId = '176'
                ";

                $stmt = sqlsrv_query($conn, $query);

                if ($stmt === false) {

                    return response()->json(array('Error' => array('Query' => sqlsrv_errors())), 200);

                }

                $counter = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

                return response()->json($counter, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getWortenAreaExitsCounter(Request $request) {

        $header = array(
            'Content-Type'  => 'application/json; charset=UTF-8',
            'charset'       => 'utf-8'
        );

        try {

            $connection_info = array("LoginTimeout"=>60, "Database" => $this->database, "UID" => $this->username, "PWD" => $this->password );
            $conn = sqlsrv_connect( $this->server_name_worten, $connection_info );

            if ($conn) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $query = "
                    SELECT
                        Counters.Exits
                    FROM
                        Counters
                    WHERE
                        Counters.CounterId = '176'
                ";

                $stmt = sqlsrv_query($conn, $query);

                if ($stmt === false) {

                    return response()->json(array('Error' => array('Query' => sqlsrv_errors())), 200);

                }

                $counter = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

                return response()->json($counter, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getVenueGatesCounters(Request $request, $venue_id) {

        $header = array(
            'Content-Type'      => 'application/json; charset=UTF-8',
            'charset'           => 'utf-8'
        );

        try {
            
            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $gates_counters_query = "
                    SELECT
                        Counters.CounterId AS counter_id,
                        Counters.Name AS name,
                        Counters.Entries AS entries,
                        Counters.Exits AS exits
                    FROM
                        Counters
                    LEFT JOIN 
                        Counters2Locations ON (
                            Counters2Locations.CounterId = Counters.CounterId
                        )
                    LEFT JOIN
                        Gates ON (
                            Gates.GateId = Counters2Locations.LocationId
                        )
                    LEFT JOIN
                        Layouts ON (
                            Layouts.LayoutId = Gates.LayoutId
                        )
                    LEFT JOIN
                        Venues ON (
                            Venues.VenueId = Layouts.VenueId
                        )
                    WHERE
                        Venues.VenueId = " . $venue_id . "
                ";

                $stmt = sqlsrv_query($conn, $gates_counters_query);

                $counters = array();

                while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                    array_push($counters, $row);

                }

                sqlsrv_free_stmt( $stmt );

                return response()->json($counters, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $e) {
            
            return response()->json(array('Errors' => array('error' => 'Internal Server Error')), 500);

        }

    }

    public function getCheckpoints(Request $request, $venue_id) {

        $header = array(
            'Content-Type'      => 'application/json; charset=UTF-8',
            'charset'           => 'utf-8'
        );

        try {
            
            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $checkpoints_query = "
                    SELECT
                        Counters.CounterId AS counter_id,
                        Counters.Name AS name,
                        Counters.Entries AS entries,
                        Counters.Exits AS exits,
                        (
                            SELECT
                                Locations.Name
                            FROM
                                Locations
                            WHERE
                                Locations.LocationId = Gates.GateId
                        ) AS parent_name,
                        Checkpoints.Configuration AS configuration
                    FROM
                        Counters
                    LEFT JOIN 
                        Counters2Locations ON (
                            Counters2Locations.CounterId = Counters.CounterId
                        )
                    LEFT JOIN
                        Checkpoints ON (
                            Checkpoints.CheckpointId = Counters2Locations.LocationId
                        )
                    LEFT JOIN
                        Checkpoints2Gates ON (
                            Checkpoints2Gates.CheckpointId = Checkpoints.CheckpointId
                        )
                    LEFT JOIN
                        Gates ON (
                            Gates.GateId = Checkpoints2Gates.GateId
                        )
                    LEFT JOIN
                        Layouts ON (
                            Layouts.LayoutId = Gates.LayoutId
                        )
                    LEFT JOIN
                        Venues ON (
                            Venues.VenueId = Layouts.VenueId
                        )
                    WHERE
                        Venues.VenueId = " . $venue_id . "
                ";

                $stmt = sqlsrv_query($conn, $checkpoints_query);

                $checkpoints = array();

                while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                    array_push($checkpoints, $row);

                }

                sqlsrv_free_stmt( $stmt );

                return response()->json($checkpoints, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $e) {
            
            return response()->json(array('Errors' => array('error' => 'Internal Server Error')), 500);

        }

    }

    public function getVenuePeakData(Request $request, $venue_id) {

        $header = array(
            'Content-Type'      => 'application/json; charset=UTF-8',
            'charset'           => 'utf-8'
        );

        try {
            
            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $gates_counters_query = "
                    SELECT
                        Counters.PeakFlowEntry AS peak_entry,
                        Counters.PeakFlowExit AS peak_exit,
                        Counters.PeakFlowTotal AS peak_total,
                        CONVERT(varchar, Counters.PeakFlowEntryTime, 120) AS peak_entry_time,
                        CONVERT(varchar, Counters.PeakFlowExitTime, 120) AS peak_exit_time,
                        CONVERT(varchar, Counters.PeakFlowTime, 120) AS peak_time
                    FROM
                        Counters
                    LEFT JOIN 
                        Counters2Locations ON (
                            Counters2Locations.CounterId = Counters.CounterId
                        )
                    LEFT JOIN
                        Locations ON (
                            Locations.LocationId = Counters2Locations.LocationId
                        )
                    LEFT JOIN
                        Venues ON (
                            Venues.VenueId = Locations.LocationId
                        )
                    WHERE
                        Venues.VenueId = " . $venue_id . "
                ";

                $stmt = sqlsrv_query($conn, $gates_counters_query);

                $counters = array();

                while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                    array_push($counters, $row);

                }

                sqlsrv_free_stmt( $stmt );

                return response()->json($counters, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $e) {
            
            return response()->json(array('Errors' => array('error' => 'Internal Server Error')), 500);

        }

    }

    public function getCountersHourly(Request $request) {

        $header = array(
            'Content-Type'  =>  'application/json; charset=UTF-8',
            'charset'       =>  'utf-8'
        );
        
        /*
    	$response = array(
    		array('Hourly' => 12, 'Total' => 2751),
    		array('Hourly' => 13, 'Total' => 1012),
    		array('Hourly' => 14, 'Total' => 2543),
    		array('Hourly' => 15, 'Total' => 1320),
    		array('Hourly' => 16, 'Total' => 945),
    		array('Hourly' => 17, 'Total' => 1750),
    		array('Hourly' => 18, 'Total' => 1078),
    		array('Hourly' => 19, 'Total' => 9754),
    		array('Hourly' => 20, 'Total' => 6213),
    		array('Hourly' => 21, 'Total' => 1325),
    		array('Hourly' => 22, 'Total' => 7520),
    		array('Hourly' => 23, 'Total' => 1252),
    		array('Hourly' => 24, 'Total' => 761),
    		array('Hourly' => 0, 'Total' => 137),
    		array('Hourly' => 1, 'Total' => 155),
    		array('Hourly' => 2, 'Total' => 704)
    	);

    	return response()->json($response, 200);
        */
        
        try {

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                if (date("H") < 4) {

                    // ORIGINAL QUERY UNCOMMENT BEFORE "PRODUCTION"
                    /*

                    $sql = "
                        SELECT
                            DATEPART(HOUR, Transactions.TransactionTime) AS hourly,
                            COUNT(Transactions.TransactionId) AS total,
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        FROM
                            Transactions
                            LEFT JOIN
                                Counters2Locations
                                ON (Counters2Locations.LocationId = Transactions.GateId)
                            LEFT JOIN
                                Counters
                                ON (Counters.CounterId = Counters2Locations.CounterId)
                        WHERE
                            Transactions.TransactionTime > " . date('Y-m-d', strtotime("- 1 days")) . "
                            AND
                            Transactions.TransactionTime < " . date('Y-m-d', strtotime("+ 2 days")) . "
                        GROUP BY
                            DATEPART(HOUR, Transactions.TransactionTime),
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        ORDER BY
                            hourly ASC
                    ";
                    
                    */

                    $sql = "
                        SELECT
                            DATEPART(HOUR, Transactions.TransactionTime) AS hourly,
                            COUNT(Transactions.TransactionId) AS total,
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        FROM
                            Transactions
                            LEFT JOIN
                                Counters2Locations
                                ON (Counters2Locations.LocationId = Transactions.GateId)
                            LEFT JOIN
                                Counters
                                ON (Counters.CounterId = Counters2Locations.CounterId)
                        WHERE
                            Transactions.TransactionTime > '2018-04-14'
                            AND
                            Transactions.TransactionTime < '2018-04-17'
                        GROUP BY
                            DATEPART(HOUR, Transactions.TransactionTime),
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        ORDER BY
                            hourly ASC
                    ";

                } else {

                    // ORIGINAL QUERY UNCOMMENT BEFORE "PRODUCTION"
                    /*

                    $sql = "
                        SELECT
                            DATEPART(HOUR, Transactions.TransactionTime) AS hourly,
                            COUNT(Transactions.TransactionId) AS total,
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        FROM
                            Transactions
                            LEFT JOIN
                                Counters2Locations
                                ON (Counters2Locations.LocationId = Transactions.GateId)
                            LEFT JOIN
                                Counters
                                ON (Counters.CounterId = Counters2Locations.CounterId)
                        WHERE
                            Transactions.TransactionTime > " . date('Y-m-d', strtotime("- 1 days")) . "
                            AND
                            Transactions.TransactionTime < " . date('Y-m-d', strtotime("+ 1 days")) . "
                            AND
                            DATEPART(HOUR, TransactionTime) > '4'
                        GROUP BY
                            DATEPART(HOUR, Transactions.TransactionTime),
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        ORDER BY
                            hourly ASC
                    ";

                    */

                    $sql = "
                        SELECT
                            DATEPART(HOUR, Transactions.TransactionTime) AS hourly,
                            COUNT(Transactions.TransactionId) AS total,
                            Transactions.GateId,
                            Transactions.VenueEntry,
                            Counters.Name
                        FROM
                            Transactions
                            LEFT JOIN
                                Counters2Locations
                                ON (Counters2Locations.LocationId = Transactions.GateId)
                            LEFT JOIN
                                Counters
                                ON (Counters.CounterId = Counters2Locations.CounterId)
                        WHERE
                            Transactions.TransactionTime > '2018-04-14'
                            AND
                            Transactions.TransactionTime < '2018-04-16'
                            AND
                            DATEPART(HOUR, TransactionTime) > '4'
                        GROUP BY
                            DATEPART(HOUR, Transactions.TransactionTime),
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        ORDER BY
                            hourly ASC
                    ";

                }


                $stmt = sqlsrv_query($conn, $sql);

                if ($stmt === false) {

                    return response()->json(array('Error' => array('Query' => sqlsrv_errors())), 200);

                }

                $counters = array();

                while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                    array_push($counters, $row);

                }

                sqlsrv_free_stmt( $stmt );

                //return response()->json(array('Counters' => $counters), 200);

		       return response()->json($counters, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getVenueTotalCounter(Request $request, $venue_id) {

        $header = array(
            'Content-Type'      => 'application/json; charset=UTF-8',
            'charset'           => 'utf-8'
        );

        try {

            $validate_venue = $this->validateVenue($venue_id);

            if (!$validate_venue || isset($validate_venue['error'])) {

                /*
    
                    Temp Solution For SMM Sines 2018 ... Need To Check Response On APP To Show Message "Connection Unavailable"
    
                */

                //return response()->json(array(array("total" => "0")), 200);


                Log::info('timeout x');



                return response()->json(array('Error' => $validate_venue['error']), 200);

            }

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if ( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $venue_counter_id_query = "
                  SELECT
                    CounterId
                  FROM
                    Counters2Locations
                  WHERE
                    LocationId = " . $venue_id . "
                ";

                $stmt = sqlsrv_query($conn, $venue_counter_id_query);

                if ($stmt === false) {

                    Log::info('[x] ... ERROR 600');
                    Log::info(sqlsrv_errors());

                    return response()->json(array('error' => array('Query' => 'Query Malformed')), 200);

                }

                if (sqlsrv_fetch($stmt) === false ) {

                    Log::info('[x] ... ERROR 601');
                    Log::info(sqlsrv_errors());

                    return response()->json(array('error' => array('Query' => 'Query Malformed')), 200);

                }

                $venue_counter_id = sqlsrv_get_field($stmt, 0);

                $venue_counter_query = "
                    SELECT
                      (Entries - Exits) AS total
                    FROM
                      Counters
                    WHERE
                      CounterId = " . $venue_counter_id . "
                ";

                $query_result = sqlsrv_query($conn, $venue_counter_query);

                if ($query_result === false) {

                    Log::info(sqlsrv_errors());

                    return response()->json(array('error' => array('Query' => 'Query Malformed')), 200);

                }

                $total_counter = sqlsrv_fetch_array($query_result, SQLSRV_FETCH_ASSOC);

                return response()->json(array($total_counter), 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getCounters(Request $request) {

        $header = array(
            'Content-Type'  =>  'application/json; charset=UTF-8',
            'charset'       =>  'utf-8'
        );

        try {

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $sql = "
                  SELECT
                    Counters.CounterId,
                    Counters.Name,
                    Counters.Entries,
                    Counters.Exits,
                    Locations.LocationType
                  FROM 
                    Counters
                    LEFT JOIN
                      Counters2Locations ON Counters.CounterId = Counters2Locations.CounterId
                    LEFT JOIN
                      Locations ON Counters2Locations.LocationId = Locations.LocationId
                  ORDER BY Counters.CounterId ASC
                ";

                $stmt = sqlsrv_query($conn, $sql);

                if ($stmt === false) {

                    return response()->json(array('Error' => array('Query' => sqlsrv_errors())), 200);

                }

                $counters = array();

                while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                    array_push($counters, $row);

                }

                sqlsrv_free_stmt( $stmt );

                return response()->json(array('Success' => array('Result' => $counters)), 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getWortenCurrentCounter(Request $request) {

        $header = array(
            'Content-Type'  =>  'application/json; charset=UTF-8',
            'charset'       =>  'utf-8'
        );

        try {

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name_worten, $connection_info);

            if( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $sql = "
                    SELECT
                        (Counters.Entries - Counters.Exits) AS current_entries
                    FROM
                        Counters
                    WHERE
                        Counters.CounterId = '176'
                ";

                $stmt = sqlsrv_query($conn, $sql);

                if ($stmt === false) {

                    return response()->json(array('Error' => array('Query' => sqlsrv_errors())), 200);

                }

                $counters = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

                sqlsrv_free_stmt( $stmt );

                return response()->json($counters, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    public function getWortenCounters(Request $request) {

        $header = array(
            'Content-Type'  =>  'application/json; charset=UTF-8',
            'charset'       =>  'utf-8'
        );

        try {

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name_worten, $connection_info);

            if( $conn ) {

                if ($conn === false) {

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $sql = "
                    SELECT
                        Counters.Entries,
                        Counters.Exits,
                        (Counters.Entries - Counters.Exits) AS current_entries
                    FROM
                        Counters
                    WHERE
                        Counters.CounterId = '176'
                ";

                $stmt = sqlsrv_query($conn, $sql);

                if ($stmt === false) {

                    return response()->json(array('Error' => array('Query' => sqlsrv_errors())), 200);

                }

                $counters = sqlsrv_fetch_array($stmt, SQLSRV_FETCH_ASSOC);

                return response()->json($counters, 200);

            } else {

                return response()->json(array('Error' => array('Message' => 'Connection could not be established.')), 200);

            }

        } catch (Exception $exception) {

            return response()->json(array('Errors' => array('Error' => 'Internal Server Error')), 500);

        }

    }

    private function validateVenue($venue_id) {

        $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
        $conn = sqlsrv_connect( $this->server_name, $connection_info);

        if ( $conn ) {

            if ($conn === false) {

                Log::info('timeout');

                return array('error' => 'Cannot Reach The Host');

            }

            $venue_counter_id_query = "
              SELECT
                VenueId
              FROM
                Venues
              WHERE
                VenueId = " . (int)$venue_id . "
            ";

            $query = sqlsrv_query($conn, $venue_counter_id_query);

            if ($query === false) {

                Log::info('[x] ... ERROR 599');
                Log::info(sqlsrv_errors());

                return array('error' => array('Query' => 'Query Malformed'));

            }

            $venue_exists = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC);

            return (isset($venue_exists["VenueId"]) ? $venue_exists["VenueId"] : array('error' => 'Venue Unavailable'));

        } else {

            return array('error' => 'Connection could not be established.');

        }

    }

    private function validateLayout($layout_id) {

        $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
        $conn = sqlsrv_connect( $this->server_name, $connection_info);

        if ( $conn ) {

            if ($conn === false) {

                return array('error' => 'Cannot Reach The Host');

            }

            $layout_id_query = "
              SELECT
                LayoutId
              FROM
                Layouts
              WHERE
                LayoutId = " . (int)$layout_id . "
            ";

            $query = sqlsrv_query($conn, $layout_id_query);

            if ($query === false) {

                Log::info('[x] ... ERROR 599');
                Log::info(sqlsrv_errors());

                return array('error' => array('Query' => 'Query Malformed'));

            }

            $template_exists = sqlsrv_fetch_array($query, SQLSRV_FETCH_ASSOC);

            return (isset($template_exists["LayoutId"]) ? $template_exists["LayoutId"] : array('error' => 'Layout Unavailable'));

        } else {

            return array('error' => 'Connection could not be established.');

        }

    }

    private function getTemplatesQuery($venue_id) {

        $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
        $conn = sqlsrv_connect( $this->server_name, $connection_info);

        if ( $conn ) {

            if ($conn === false) {

                return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

            }

            $layouts_query = "
              SELECT
                Layouts.LayoutId,
                L.Name
              FROM
                Layouts
                LEFT JOIN
                Locations L on Layouts.LayoutId = L.LocationId
              WHERE
                VenueId = " . $venue_id . "
            ";

            $stmt = sqlsrv_query($conn, $layouts_query);

            $layouts = array();

            while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                array_push($layouts, $row);

            }

            sqlsrv_free_stmt( $stmt );

            return $layouts;

        } else {

            return array('Error' => array('Message' => 'Connection could not be established.'));

        }

    }

    private function getCountersHourlyByGate($counter_id) {

        try {

            $connection_info = array("LoginTimeout"=>60, "Database"=>$this->database, "UID"=>$this->username, "PWD"=>$this->password);
            $conn = sqlsrv_connect( $this->server_name, $connection_info);

            if( $conn ) {

                if ($conn === false) {

                    Log::info('timeout');

                    return response()->json(array('Error' => array('Connection' => sqlsrv_errors())), 200);

                }

                $field = '';

                switch ($counter_id) {
                    case '199':
                        $field = 'Entries';
                        break;
                    case '200':
                        $field = 'Exits';
                        break;
                    default:
                        $field = 'Entries';
                        break;
                }

                $sql = "
                        SELECT
                            " . $field . " AS value
                        FROM
                            Counters
                        WHERE
                            CounterId = " . (int)$counter_id . "
                    ";                

                /*
                if (date("H") < 4) {

                    // ORIGINAL QUERY UNCOMMENT BEFORE "PRODUCTION"
                    
                    /*
                    $sql = "
                        SELECT
                            DATEPART(HOUR, Transactions.TransactionTime) AS hourly,
                            COUNT(Transactions.TransactionId) AS total,
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        FROM
                            Transactions
                            LEFT JOIN
                                Counters2Locations
                                ON (Counters2Locations.LocationId = Transactions.GateId)
                            LEFT JOIN
                                Counters
                                ON (Counters.CounterId = Counters2Locations.CounterId)
                        WHERE
                            Transactions.TransactionTime > " . date('Y-m-d', strtotime("- 1 days")) . "
                            AND
                            Transactions.TransactionTime < " . date('Y-m-d', strtotime("+ 2 days")) . "
                            AND
                            Transactions.GateId = " . (int)$gate_id . " 
                        GROUP BY
                            DATEPART(HOUR, Transactions.TransactionTime),
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        ORDER BY
                            hourly ASC
                    ";
                    */

                    /*

                    $sql = "
                        SELECT
                            DATEPART(HOUR, Transactions.TransactionTime) AS hourly,
                            COUNT(Transactions.TransactionId) AS total
                        FROM
                            Transactions
                            LEFT JOIN
                                Counters2Locations
                                ON (Counters2Locations.LocationId = Transactions.GateId)
                            LEFT JOIN
                                Counters
                                ON (Counters.CounterId = Counters2Locations.CounterId)
                        WHERE
                            Transactions.TransactionTime > '2018-04-14'
                            AND
                            Transactions.TransactionTime < '2018-04-17'
                            AND
                            Transactions.GateId = 145
                        GROUP BY
                            DATEPART(HOUR, Transactions.TransactionTime),
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        ORDER BY
                            hourly ASC
                    ";
                    */

                /*} else {

                    // ORIGINAL QUERY UNCOMMENT BEFORE "PRODUCTION"
                    
                    /*
                    $sql = "
                        SELECT
                            DATEPART(HOUR, Transactions.TransactionTime) AS hourly,
                            COUNT(Transactions.TransactionId) AS total
                        FROM
                            Transactions
                            LEFT JOIN
                                Counters2Locations
                                ON (Counters2Locations.LocationId = Transactions.GateId)
                            LEFT JOIN
                                Counters
                                ON (Counters.CounterId = Counters2Locations.CounterId)
                        WHERE
                            Transactions.TransactionTime > " . date('Y-m-d', strtotime("- 1 days")) . "
                            AND
                            Transactions.TransactionTime < " . date('Y-m-d', strtotime("+ 1 days")) . "
                            AND
                            DATEPART(HOUR, TransactionTime) > '4'
                            AND
                            Transactions.GateId = " . (int)$gate_id . " 
                        GROUP BY
                            DATEPART(HOUR, Transactions.TransactionTime),
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        ORDER BY
                            hourly ASC
                    ";
                    */

                    /*
                    $sql = "
                        SELECT
                            DATEPART(HOUR, Transactions.TransactionTime) AS hourly,
                            COUNT(Transactions.TransactionId) AS total
                        FROM
                            Transactions
                            LEFT JOIN
                                Counters2Locations
                                ON (Counters2Locations.LocationId = Transactions.GateId)
                            LEFT JOIN
                                Counters
                                ON (Counters.CounterId = Counters2Locations.CounterId)
                        WHERE
                            Transactions.TransactionTime > '2018-04-14'
                            AND
                            Transactions.TransactionTime < '2018-04-16'
                            AND
                            DATEPART(HOUR, TransactionTime) > '4'
                            AND
                            Transactions.GateId = 145              
                        GROUP BY
                            DATEPART(HOUR, Transactions.TransactionTime),
                            Transactions.VenueEntry,
                            Transactions.GateId,
                            Counters.Name
                        ORDER BY
                            hourly ASC
                    ";*/

                /*}
                */

                $stmt = sqlsrv_query($conn, $sql);

                if ($stmt === false) {

                    return response()->json(array('Error' => array('Query' => sqlsrv_errors())), 200);

                }

                $counters = array();

                while ( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

                    array_push($counters, $row);

                }

                sqlsrv_free_stmt( $stmt );

                //return response()->json(array('Counters' => $counters), 200);

               return $counters;

                //return response()->json($counters), 200);

            } else {

                return array('Error' => array('Message' => 'Connection could not be established.'));

            }

        } catch (Exception $exception) {

            return array('Errors' => array('Error' => 'Internal Server Error'));

        }

    }

    public function VenueCountersHourly(Request $request, $venue_id) {

        $header = array(
            'Content-Type'      => 'application/json; charset=UTF-8',
            'charset'           => 'utf-8'
        );

        try {

            $validate_venue = $this->validateVenue($venue_id);

            if (!$validate_venue || isset($validate_venue['error'])) {

                $gates = [
                    [
                        "label" => "Entradas",
                        "total"  => 0
                        /*"value" => [
                            [
                                //"hourly" => 17,
                                "total"  => 0
                            ]
                        ]*/
                    ],
                    [
                        "label" => "Saídas",
                        "total"  => 0
                        /*"value" => [
                            [
                                //"hourly" => 18,
                                "total"  => 0
                            ]
                        ]*/
                    ]
                ];

                return response()->json($gates, 200);

                return response()->json(array('Error' => $validate_venue['error']), 200);

            }

            /* Get Venue Templates */

            /* $templates = $this->getTemplatesQuery($venue_id); */

            /* Get Gates Of Layouts / Templates */

            /* Function not defined yet ... Must be created */

             /*

                Temp Solution For FMM Sines 2018

            */

            $layout_id = 174;

            $gates = [
                ["id" => 176, "counter_id" => 199 , "description" => "Entrada"],
                ["id" => 177, "counter_id" => 200 , "description" => "Saída"],
            ];

            $chart_lines = array();

            foreach ($gates as $key) {
                
                $counter = $this->getCountersHourlyByGate($key['counter_id']);

                $value = $counter[0]['value'];

                array_push($chart_lines, ['label' => $key['description'], 'value' => $value]);

            }

            return $chart_lines;

        } catch (Exception $exception) {

            return array('Errors' => array('Error' => 'Internal Server Error'));

        }


    }

}