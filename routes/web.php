<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return response()->json(array('data' => 'Handshake Counter API V0.01 BETA' ), 200);
});

$app->group(['prefix' => 'venues'], function ($app) {
    $app->get('all', 'CounterController@getVenues');
    $app->get('associate', 'CounterController@getVenuesToCustomer');
});

//$app->group(['prefix' => 'layouts'], function($app){
//});

$app->group(['prefix' => 'venue'], function ($app) {
    $app->get('{venue_id}/layouts', 'CounterController@getVenueLayouts');
    $app->get('{venue_id}/total', 'CounterController@getVenueTotalCounter');
    $app->get('{venue_id}/templates', 'CounterController@getTemplates');
    $app->get('{venue_id}/counters/hourly', 'CounterController@VenueCountersHourly');
    $app->get('{venue_id}/gates/counters', 'CounterController@getVenueGatesCounters');
    $app->get('{venue_id}/checkpoints', 'CounterController@getCheckpoints');
    $app->get('{venue_id}/peak_data', 'CounterController@getVenuePeakData');
});

$app->group(['prefix' => 'access'], function ($app) {
    $app->get('counters', 'CounterController@getCounters');
    $app->get('area', 'CounterController@getAreaCounter');
    $app->get('counters_hourly', 'CounterController@getCountersHourly');
});

$app->group(['prefix' => 'worten'], function ($app) {
    $app->get('access/area', 'CounterController@getWortenAreaEntryCounter');
    $app->get('exits/area', 'CounterController@getWortenAreaExitsCounter');
    $app->get('current/counters', 'CounterController@getWortenCurrentCounter');
    $app->get('counters', 'CounterController@getWortenCounters');
});